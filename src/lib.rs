//! Helper to index files under a directory and check if their contents have been modified.
//!
//! Useful for media libraries, build systems, etc.

// /run/udev/data/b259:0

mod diff;
mod fsid;
mod index_serde;
mod scan;
mod scan_options;

use std::{
	collections::{
		BTreeMap,
		HashMap,
		HashSet,
	},
	path::{
		Path,
		PathBuf,
	},
	time::SystemTime,
};

use serde::{
	Deserialize,
	Serialize,
};

pub use crate::{
	diff::Difference,
	scan_options::{
		HashMode,
		ScanOptions,
	},
};

/// Record of a scanned file.
#[derive(Serialize, Deserialize)]
#[serde(bound(
	serialize = "FA: serde::Serialize",
	deserialize = "FA: serde::Deserialize<'de>"
))]
pub struct FileRecord<H: digest::Digest, FA> {
	/// Path to the file, relative to the scan root.
	pub path: PathBuf,
	/// Last known modification time of the file.
	pub mtime: SystemTime,
	/// Last known size of the file, in bytes.
	pub size: u64,
	/// Filesystem identifier and inode number, if recorded.
	///
	/// Can help track files through renames as well as detect hard links, but is not available for
	/// all filesystems.
	///
	/// The identifier is usually the filesystem's UUID, though it may be other things for non-standard
	/// filesystems.
	pub drive_inode: Option<(String, u64)>,
	/// Hash hash of the file, if recorded.
	#[serde(
		serialize_with = "index_serde::ser_opt_digest_output::<_,H>",
		deserialize_with = "index_serde::de_opt_digest_output::<_,H>"
	)]
	pub hash: Option<digest::Output<H>>,

	/// Application-dependent auxillary data
	pub aux: FA,
}
impl<H: digest::Digest, FA> FileRecord<H, FA> {
	/// Test if two files likely have the same content.
	///
	/// If both records have the content hashes available, they are checked for a definitive match.
	/// Otherwise, the files are assumed to have the same content if their sizes and mtimes match, and
	/// they either have the same drive+inode identifiers (if available on both records) or same file path.
	///
	/// The file records do not have to be from the same index.
	pub fn likely_same_content<FB>(&self, other: &FileRecord<H, FB>) -> bool {
		// Easy test first - if sizes are different, they definitely are not the same.
		if self.size != other.size {
			return false;
		}

		if let (Some(shash), Some(ohash)) = (&self.hash, &other.hash) {
			// Hashes available on both files. Check them for a definitive test
			if shash == ohash {
				return true;
			}
		}

		// Hashes unavailable, use size+mtime matching. Size is checked above.

		if self.mtime != other.mtime {
			// mtime different, probably modified
			return false;
		}

		if let (Some(sdi), Some(odi)) = (&self.drive_inode, &other.drive_inode) {
			// Drive+inode information available on both files. Compare them, as they can track across renames
			if sdi == odi {
				return true;
			}
		}

		// Hash and drive+inode info unavailable, so only assume unchanged if they are the same path
		self.path == other.path
	}
}
impl<H: digest::Digest, FA> Clone for FileRecord<H, FA>
where
	digest::Output<H>: Clone,
	FA: Clone,
{
	// manual clone implementation that doesn't unneccessairly require `H: Clone`
	fn clone(&self) -> Self {
		Self {
			path: self.path.clone(),
			mtime: self.mtime.clone(),
			size: self.size.clone(),
			drive_inode: self.drive_inode.clone(),
			hash: self.hash.clone(),
			aux: self.aux.clone(),
		}
	}
}
impl<H: digest::Digest, FA> std::fmt::Debug for FileRecord<H, FA>
where
	digest::Output<H>: std::fmt::Debug,
{
	// manual debug implementation that doesn't unneccessairly require `H: Debug`
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("FileRecord")
			.field("path", &self.path)
			.field("mtme", &self.mtime)
			.field("size", &self.size)
			.field("drive_inode", &self.drive_inode)
			.field("hash", &self.hash)
			.finish()
	}
}
/// `PartialEq` for `FileRecord` compares the record's fields. If you want to check if two records likely refer
/// to the same content, use [`FileRecord::likely_same_content`]
impl<H: digest::Digest, FA> PartialEq for FileRecord<H, FA> {
	// manual partial eq implementation that doesn't unneccessairly require `H: PartialEq`
	fn eq(&self, other: &Self) -> bool {
		self.path == other.path
			&& self.mtime == other.mtime
			&& self.size == other.size
			&& self.drive_inode == other.drive_inode
			&& self.hash == other.hash
	}
}
/// `Eq` for `FileRecord` compares the record's fields. If you want to check if two records likely refer
/// to the same content, use [`FileRecord::likely_same_content`]
impl<H: digest::Digest, FA> Eq for FileRecord<H, FA> {}

/// Record of a scanned directory.
///
/// This is only used for directory mtime optimization in [`ScanOptions::use_directory_mtime`], nothing more.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct DirRecord<DA> {
	/// Path
	pub path: PathBuf,
	/// Last known modification time of the directory
	pub mtime: SystemTime,

	/// Application-dependent auxillary data
	pub aux: DA,
}

pub type IndexFor<SO> =
	Index<<SO as ScanOptions>::Digest, <SO as ScanOptions>::FileAux, <SO as ScanOptions>::DirAux>;

/// Index of scanned files.
///
/// The type argument is the hash to use to compare file contents, if enabled in the scan. Using a hash with a low
/// collision rate, such as a cryptographically secure hash like SHA2 or 3, is highly recommended to avoid false
/// positives.
#[derive(Clone, Debug)]
pub struct Index<H: digest::Digest, FA = (), DA = ()> {
	by_path: BTreeMap<PathBuf, FileRecord<H, FA>>,
	by_hash: HashMap<digest::Output<H>, HashSet<PathBuf>>,
	by_drive_inode: HashMap<(String, u64), HashSet<PathBuf>>,
	directores_by_path: HashMap<PathBuf, DirRecord<DA>>,
}
impl<H: digest::Digest, FA, DA> Index<H, FA, DA> {
	fn new<FI, DI>(files_iter: FI, directories_iter: DI) -> Self
	where
		FI: IntoIterator<Item = FileRecord<H, FA>>,
		DI: IntoIterator<Item = DirRecord<DA>>,
	{
		let files_iter = files_iter.into_iter();
		let directories_iter = directories_iter.into_iter();
		let mut this = Self {
			by_path: BTreeMap::new(),
			by_hash: HashMap::new(),
			by_drive_inode: HashMap::new(),
			directores_by_path: directories_iter
				.into_iter()
				.map(|v| (v.path.clone(), v))
				.collect(),
		};

		for item in files_iter {
			if let Some(ref hash) = item.hash {
				this.by_hash
					.entry(hash.clone())
					.or_default()
					.insert(item.path.clone());
			}
			if let Some(ref dev_ino) = item.drive_inode {
				this.by_drive_inode
					.entry(dev_ino.clone())
					.or_default()
					.insert(item.path.clone());
			}
			this.by_path.insert(item.path.clone(), item);
		}

		this
	}

	/// Recursively scans a directory and creates an index.
	///
	/// See [`ScanOptions`] for options to control scanning.
	pub fn scan<O>(path: &Path, options: &O) -> Self
	where
		O: ScanOptions<Digest = H, FileAux = FA, DirAux = DA>,
	{
		scan::scan(path, Path::new(""), options)
	}

	/// Gets the differences from this index and an older one.
	pub fn diff<'iter, 'old: 'iter, 'new: 'iter>(
		&'new self,
		old: &'old Self,
	) -> impl Iterator<Item = Difference<'old, 'new>> + 'iter {
		diff::diff(old, self)
	}

	/// Gets the indexed file data by its path, relative to the index's start without a "./" prefix.
	pub fn by_path(&self, path: impl AsRef<Path>) -> Option<&FileRecord<H, FA>> {
		self.by_path.get(path.as_ref())
	}

	/// Looks up a file by its hash digest.
	///
	/// As multiple files can contain the same content, this returns an iterator iterating over all
	/// files with the hash.
	pub fn by_digest<'a>(
		&'a self,
		hash: &digest::Output<H>,
	) -> impl Iterator<Item = &'a FileRecord<H, FA>> {
		self.by_hash
			.get(hash)
			.map(|paths| paths.iter().map(|p| self.by_path.get(p).unwrap()))
			.map(either::Either::Left)
			.unwrap_or(either::Either::Right(std::iter::empty()))
	}

	/// Looks up a file by its drive + inode info.
	///
	/// As multiple files can share the same inode via hard links, this returns an iterator iterating
	/// over all found hard links to the same inode.
	pub fn by_drive_inode<'a>(
		&'a self,
		drive: &str,
		ino: u64,
	) -> impl Iterator<Item = &'a FileRecord<H, FA>> {
		// TODO: std HashMap with tuples require all fields to be owned, so we have to copy the drive string,
		// which isn't great for performance.
		self.by_drive_inode
			.get(&(drive.into(), ino))
			.map(|paths| paths.iter().map(|p| self.by_path.get(p).unwrap()))
			.map(either::Either::Left)
			.unwrap_or(either::Either::Right(std::iter::empty()))
	}

	/// Finds a file that likely has the same content as the one from the passed-in record.
	///
	/// The record need not be from the index - in fact, this method is useful for seeing if a removed file from an old index is still available,
	/// because it had been moved or another copy was available.
	///
	/// In the case of multiple matching files, the search first prefers the file in the same path, then a file with the same drive+inode
	/// identifier, then any file with the same hash.
	pub fn by_same_content<FB>(&self, test: &FileRecord<H, FB>) -> Option<&FileRecord<H, FA>> {
		let candidates = [
			self.by_path.get(&test.path),
			test.drive_inode
				.as_ref()
				.and_then(|di| self.by_drive_inode.get(di))
				.and_then(|hs| hs.iter().next())
				.and_then(|p| self.by_path.get(p)),
			test.hash
				.as_ref()
				.and_then(|id| self.by_hash.get(id))
				.and_then(|hs| hs.iter().next())
				.and_then(|p| self.by_path.get(p)),
		];

		candidates
			.into_iter()
			.filter_map(|v| v)
			.find(|fr| fr.likely_same_content(test))
	}

	/// Iterates over all discovered files.
	pub fn iter<'a>(&'a self) -> impl Iterator<Item = &'a FileRecord<H, FA>> {
		self.by_path.values()
	}

	/// Returns the number of files scanned
	pub fn len(&self) -> usize {
		self.by_path.len()
	}

	/// Gets the directory records.
	///
	/// Directories are only tracked for the purpose of directory mtime optimization.
	pub fn directories(&self) -> &HashMap<PathBuf, DirRecord<DA>> {
		&self.directores_by_path
	}
}
