use std::{
	collections::HashMap,
	ffi::{
		c_ulong,
		OsString,
	},
	fs::Metadata,
	os::unix::prelude::{
		MetadataExt,
		OsStringExt,
	},
	path::Path,
	process::{
		Command,
		Stdio,
	},
};

use nix::sys::statvfs::statvfs;

pub struct FSIDCache {
	by_dev_t: HashMap<u64, Option<String>>,
	by_statvfs_id: HashMap<c_ulong, Option<String>>,

	mtab_by_statvfs_id: HashMap<c_ulong, MtabEntry>,
}
impl FSIDCache {
	pub fn new() -> Self {
		let mtab = parse_mtab()
			.map_err(|e| log::warn!("Could not parse mtab: {}", e))
			.unwrap_or_default();

		let mtab_by_statvfs_id = mtab
			.into_iter()
			.filter_map(|entry| {
				let stat = statvfs(entry.mount_path.as_os_str())
					.map_err(|e| log::warn!("Could not statvfs {:?}: {}", entry.mount_path, e))
					.ok()?;
				Some((stat.filesystem_id(), entry))
			})
			.collect();

		Self {
			by_dev_t: HashMap::new(),
			by_statvfs_id: HashMap::new(),
			mtab_by_statvfs_id,
		}
	}

	pub fn get(&mut self, path: &Path, md: &Metadata) -> Option<String> {
		let dev_t = md.dev();
		if let Some(id) = self.by_dev_t.get(&md.dev()) {
			return id.clone();
		}

		let fsid = statvfs(path)
			.map_err(|e| log::warn!("Could not statvfs {:?}: {}", path, e))
			.ok()
			.map(|fstat| fstat.filesystem_id());
		if let Some(id) = fsid.and_then(|fsid| self.by_statvfs_id.get(&fsid)) {
			return id.clone();
		}

		let uuid = uuid_of_dev(dev_t)
			.map_err(|e| log::warn!("Could not get uuid for dev_t {:?}: {}", dev_t, e))
			.ok()
			.flatten();

		if let Some(uuid) = uuid {
			self.by_dev_t.insert(dev_t, Some(uuid.clone()));
			return Some(uuid);
		}

		if let Some(mtab) = fsid.and_then(|fsid| self.mtab_by_statvfs_id.get(&fsid)) {
			if mtab.typ == "zfs" {
				log::trace!("Getting zfs guid for {:?}", mtab.device);
				let zfs_out = Command::new("zfs")
					.args(&["get", "-o", "value", "-H", "guid"])
					.arg(&mtab.device)
					.stdout(Stdio::piped())
					.stderr(Stdio::inherit())
					.output()
					.map_err(|e| log::warn!("Could not get zfs guid for {:?}: {}", mtab.device, e))
					.ok()
					.and_then(|res| {
						if !res.status.success() {
							log::warn!(
								"Could not get zfs guid for {:?}: zfs exited with code {}",
								mtab.device,
								res.status
							);
							None
						} else {
							Some(String::from_utf8_lossy(&res.stdout).into_owned())
						}
					});
				if let Some(zfs_out) = zfs_out {
					let id = format!("zfs:guid:{}", zfs_out.trim());
					self.by_statvfs_id.insert(fsid.unwrap(), Some(id.clone()));
					return Some(id);
				}
			}

			// TODO: are statvfs fsid's persistent across reboots/remounts/etc?
		}

		self.by_dev_t.insert(dev_t, None);
		fsid.map(|fsid| self.by_statvfs_id.insert(fsid, None));
		return None;
	}
}

struct MtabEntry {
	device: OsString,
	mount_path: OsString,
	typ: OsString,
}

fn parse_mtab() -> Result<Vec<MtabEntry>, std::io::Error> {
	let re = regex::bytes::Regex::new(r"^\s*([^ ]+) ([^ ]+) ([^ ]+)").unwrap();
	let mtab = std::fs::read("/proc/self/mounts")?;
	let out = mtab
		.split(|b| *b == b'\n')
		.filter(|line| !line.is_empty())
		.map(|line| {
			re.captures(line).unwrap_or_else(|| {
				panic!(
					"Line {:?} ({:?}) failed regex match. Either this is a bug (most likely), or Linux changed /proc/*/mounts syntax",
					String::from_utf8_lossy(line),
					line
				);
			})
		})
		.map(|capture| MtabEntry {
			device: mtab_unescape(capture.get(1).unwrap().as_bytes()),
			mount_path: mtab_unescape(capture.get(2).unwrap().as_bytes()),
			typ: mtab_unescape(capture.get(3).unwrap().as_bytes()),
		})
		.collect();
	Ok(out)
}

fn uuid_of_dev(dev: u64) -> Result<Option<String>, std::io::Error> {
	let major = nix::sys::stat::major(dev);
	let minor = nix::sys::stat::minor(dev);
	log::trace!("Getting drive UUID for {}:{}", major, minor);
	Ok(
		std::fs::read_to_string(format!("/run/udev/data/b{}:{}", major, minor))?
			.lines()
			.find(|line| {
				line.starts_with("E:ID_FS_UUID=") || line.starts_with("E:ID_PART_TABLE_UUID=")
			})
			.and_then(|line| line.split_once('=').map(|(_, uuid)| uuid.trim().to_owned())),
	)
}

fn mtab_unescape(value: &[u8]) -> OsString {
	let mut out = vec![];
	let mut it = value.iter().copied();
	while let Some(byt) = it.next() {
		if byt == b'\\' {
			let seq = [it.next().unwrap(), it.next().unwrap(), it.next().unwrap()];
			let esc_byt = u8::from_str_radix(std::str::from_utf8(&seq).unwrap(), 8).unwrap();
			out.push(esc_byt);
		} else {
			out.push(byt);
		}
	}
	OsString::from_vec(out)
}
