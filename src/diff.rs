use std::path::Path;

use crate::{
	FileRecord,
	Index,
};

/// A single difference between directory tree indices
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Difference<'o, 'n> {
	/// A file exists at this path that was not in the old index
	FileCreated(&'n Path),
	/// A file no longer exists at this path that was in the old index.
	FileRemoved(&'o Path),
	/// A file exists in both the old and new index, but has probably changed.
	FileModified(&'n Path),
}

pub(crate) fn diff<'iter, 'old: 'iter, 'new: 'iter, H, FO, DO, FN, DN>(
	old: &'old Index<H, FO, DO>,
	new: &'new Index<H, FN, DN>,
) -> impl Iterator<Item = Difference<'old, 'new>> + 'iter
where
	H: digest::Digest,
{
	old.by_path
		.values()
		.filter_map(|old_record| check_old(new, old_record))
		.chain(
			new.by_path
				.values()
				.filter_map(|new_record| check_new(old, new_record)),
		)
}

fn check_new<'n, H, FO, DO, FN>(
	old: &Index<H, FO, DO>,
	new_record: &'n FileRecord<H, FN>,
) -> Option<Difference<'static, 'n>>
where
	H: digest::Digest,
{
	if let Some(prev_record) = old.by_path.get(&new_record.path) {
		match (&prev_record.hash, &new_record.hash) {
			(Some(old), Some(new)) if old == new => {
				// Same content, guarenteed by hash
				return None;
			}
			(Some(old), Some(new)) if old != new => {
				// Different content, guarenteed by hash
				Some(Difference::FileModified(&new_record.path));
			}
			_ => (),
		}

		if prev_record.size != new_record.size || prev_record.mtime != new_record.mtime {
			return Some(Difference::FileModified(&new_record.path));
		}

		match (&prev_record.drive_inode, &new_record.drive_inode) {
			(Some(old), Some(new)) if old != new => {
				return Some(Difference::FileModified(&new_record.path));
			}
			_ => {}
		}

		None
	} else {
		Some(Difference::FileCreated(&new_record.path))
	}
}

fn check_old<'o, H, FO, FN, DN>(
	new: &Index<H, FN, DN>,
	old_record: &'o FileRecord<H, FO>,
) -> Option<Difference<'o, 'static>>
where
	H: digest::Digest,
{
	if !new.by_path.contains_key(&old_record.path) {
		Some(Difference::FileRemoved(&old_record.path))
	} else {
		None
	}
}
