//! Serde implementations and supporting functions.
//!
//! [`Index`] has several redundant maps that are convenient for lookups but
//! should not be stored, so it doesn't use the trivial derive serde implementations.

use std::{
	collections::{
		BTreeMap,
		HashMap,
		HashSet,
	},
	marker::PhantomData,
	path::PathBuf,
};

use digest::Digest;
use serde::{
	de::{
		Error as _,
		Visitor,
	},
	ser::SerializeStruct,
	Deserialize,
	Serialize,
};

use crate::{
	DirRecord,
	FileRecord,
	Index,
};

struct SerFileKeys<'a, H: Digest, FA: serde::Serialize>(&'a BTreeMap<PathBuf, FileRecord<H, FA>>);
impl<'a, H: Digest, FA: serde::Serialize> serde::Serialize for SerFileKeys<'a, H, FA> {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		serializer.collect_seq(self.0.values())
	}
}

struct SerDirKeys<'a, DA: serde::Serialize>(&'a HashMap<PathBuf, DirRecord<DA>>);
impl<'a, DA: serde::Serialize> serde::Serialize for SerDirKeys<'a, DA> {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		serializer.collect_seq(self.0.values())
	}
}

impl<H, FA, DA> serde::Serialize for Index<H, FA, DA>
where
	H: digest::Digest,
	FA: serde::Serialize,
	DA: serde::Serialize,
{
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		let mut ser = serializer.serialize_struct("Index", 2)?;
		ser.serialize_field("files", &SerFileKeys(&self.by_path))?;
		ser.serialize_field("directories", &SerDirKeys(&self.directores_by_path))?;
		ser.end()
	}
}

struct DeserFilesVisitor<H: Digest, FA>(PhantomData<fn() -> (H, FA)>);
impl<'de, H: Digest, FA: serde::Deserialize<'de>> serde::de::Visitor<'de>
	for DeserFilesVisitor<H, FA>
{
	type Value = BTreeMap<PathBuf, FileRecord<H, FA>>;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("a sequence of file records")
	}

	fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
	where
		A: serde::de::SeqAccess<'de>,
	{
		let mut value = BTreeMap::new();
		while let Some(v) = seq.next_element::<FileRecord<H, FA>>()? {
			value.insert(v.path.clone(), v);
		}
		Ok(value)
	}
}

struct DeserDirVisitor<DA>(PhantomData<fn() -> DA>);
impl<'de, DA: serde::Deserialize<'de>> serde::de::Visitor<'de> for DeserDirVisitor<DA> {
	type Value = HashMap<PathBuf, DirRecord<DA>>;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("a sequence of file records")
	}

	fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
	where
		A: serde::de::SeqAccess<'de>,
	{
		let mut value = HashMap::new();
		while let Some(v) = seq.next_element::<DirRecord<DA>>()? {
			value.insert(v.path.clone(), v);
		}
		Ok(value)
	}
}

#[derive(Deserialize)]
#[serde(bound = "FA: serde::Deserialize<'de>, DA: serde::Deserialize<'de>")]
struct IndexDeser<H, FA, DA>
where
	H: digest::Digest,
{
	#[serde(deserialize_with = "deser_files")]
	files: BTreeMap<PathBuf, FileRecord<H, FA>>,
	#[serde(deserialize_with = "deser_dirs")]
	directories: HashMap<PathBuf, DirRecord<DA>>,
}

fn deser_files<'de, D, H, FA>(de: D) -> Result<BTreeMap<PathBuf, FileRecord<H, FA>>, D::Error>
where
	D: serde::Deserializer<'de>,
	FA: serde::Deserialize<'de>,
	H: digest::Digest,
{
	de.deserialize_seq(DeserFilesVisitor::<H, FA>(Default::default()))
}

fn deser_dirs<'de, D, DA>(de: D) -> Result<HashMap<PathBuf, DirRecord<DA>>, D::Error>
where
	D: serde::Deserializer<'de>,
	DA: serde::Deserialize<'de>,
{
	de.deserialize_seq(DeserDirVisitor::<DA>(Default::default()))
}

impl<'de, H, FA, DA> serde::Deserialize<'de> for Index<H, FA, DA>
where
	H: Digest,
	FA: serde::Deserialize<'de>,
	DA: serde::Deserialize<'de>,
{
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		let ser_data = IndexDeser::deserialize(deserializer)?;
		let mut by_hash = HashMap::<_, HashSet<PathBuf>>::new();
		let mut by_drive_inode = HashMap::<_, HashSet<PathBuf>>::new();

		for rec in ser_data.files.values() {
			if let Some(hash) = rec.hash.clone() {
				by_hash.entry(hash).or_default().insert(rec.path.clone());
			}
			if let Some((ref drive, ino)) = rec.drive_inode {
				by_drive_inode
					.entry((drive.clone(), ino))
					.or_default()
					.insert(rec.path.clone());
			}
		}

		Ok(Self {
			by_path: ser_data.files,
			by_hash,
			by_drive_inode,
			directores_by_path: ser_data.directories,
		})
	}
}

pub(crate) fn ser_opt_digest_output<S: serde::Serializer, H: Digest>(
	val: &Option<digest::Output<H>>,
	ser: S,
) -> Result<S::Ok, S::Error> {
	struct SerDigestOutput<'a, H: Digest>(&'a digest::Output<H>);
	impl<'a, H: Digest> Serialize for SerDigestOutput<'a, H> {
		fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where
			S: serde::Serializer,
		{
			serializer.serialize_bytes(&*self.0)
		}
	}

	val.as_ref().map(SerDigestOutput::<H>).serialize(ser)
}

struct DigestVisitor<H: Digest>(PhantomData<fn() -> H>);
impl<'de, H: Digest> Visitor<'de> for DigestVisitor<H> {
	type Value = digest::Output<H>;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(formatter, "a {}-byte hash", <H as Digest>::output_size())
	}

	fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		if v.len() != <H as Digest>::output_size() {
			return Err(E::invalid_length(v.len(), &self));
		}

		let mut d: digest::Output<H> = Default::default();
		d.copy_from_slice(v);
		Ok(d)
	}

	fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
	where
		A: serde::de::SeqAccess<'de>,
	{
		let mut d: digest::Output<H> = Default::default();
		let mut i = 0;
		while let Some(v) = seq.next_element::<u8>()? {
			if i >= d.len() {
				// advance until end to get an accurate length
				while seq.next_element::<u8>()?.is_some() {
					i += 1;
				}
				return Err(A::Error::invalid_length(i, &self));
			}
			d[i] = v;
			i += 1;
		}
		if i != d.len() {
			return Err(A::Error::invalid_length(i, &self));
		}
		Ok(d)
	}
}

struct OptDigestVisitor<H: Digest>(PhantomData<fn() -> H>);
impl<'de, H: Digest> Visitor<'de> for OptDigestVisitor<H> {
	type Value = Option<digest::Output<H>>;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(
			formatter,
			"an optional {}-byte hash",
			<H as Digest>::output_size()
		)
	}

	fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		DigestVisitor(self.0).visit_bytes(v).map(Some)
	}

	fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		deserializer
			.deserialize_bytes(DigestVisitor(self.0))
			.map(Some)
	}

	fn visit_none<E>(self) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		Ok(None)
	}
}

pub(crate) fn de_opt_digest_output<'de, D: serde::Deserializer<'de>, H: Digest>(
	de: D,
) -> Result<Option<digest::Output<H>>, D::Error> {
	de.deserialize_option(OptDigestVisitor::<H>(Default::default()))
}
