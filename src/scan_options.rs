use std::{
	fs::Metadata,
	path::Path,
};

use crate::Index;

/// Info and callbacks for scanning
pub trait ScanOptions {
	type Digest: digest::Digest;
	type Error: From<std::io::Error>;
	type FileAux: Clone;
	type DirAux: Clone;

	/// Called when an error occurs.
	///
	/// Passed the path relative to the scanner root and the error that occured.
	///
	/// IO errors do not terminate scanning - it's up to the application to handle them.
	fn on_error(&self, rel_path: &Path, error: Self::Error);

	/// Get previous scan index. The scan will reuse data from here if it can safely do so.
	/// Defaults to None.
	fn prev_index(&self) -> Option<&Index<Self::Digest, Self::FileAux, Self::DirAux>> {
		None
	}

	/// Get when to hash files. See the type's variants for more info. Defaults to `HashMode::IfMetadataChanged`.
	fn hash_mode(&self) -> HashMode {
		HashMode::IfMetadataChanged
	}

	/// Get if the scan should use the directory's mtime to skip scanning directories.
	///
	/// If this returns true, and a directory exists in the previous index and its mtime has not changed,
	/// the scanner assumes that the descendent files and directories are unmodified.
	///
	/// Has no effect if `self.prev_index()` returns `None`. The default implementation returns `false`.
	fn use_directory_mtime(&self) -> bool {
		false
	}

	/// Filter function.
	///
	/// The scanner calls this function for each directory entry it comes across, passing in the
	/// path relative to the scan root and the `DirEntry`. If this returns `Ok(true)`,
	/// the scanner will scan the file or descend into the directory. If this returns
	/// `Ok(false)`, the file or directory is skipped and not examined further. If this
	/// returns `Err`, the file or directory is skipped and the `on_error` callback is called
	/// with the error.
	///
	/// If not specified, all files are accepted
	fn should_index(&self, rel_path: &Path, metadata: &Metadata) -> Result<bool, Self::Error> {
		let _ = rel_path;
		let _ = metadata;
		Ok(true)
	}

	/// Generate auxillary data for a file.
	fn file_aux_data(
		&self,
		rel_path: &Path,
		metadata: &Metadata,
	) -> Result<Self::FileAux, Self::Error>;

	/// Generate auxillary data for a directory.
	fn dir_aux_data(
		&self,
		rel_path: &Path,
		metadata: &Metadata,
	) -> Result<Self::DirAux, Self::Error>;
}

/// Specifies when to hash files. See the type's variants for more info.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum HashMode {
	/// Always compute the hash when indexing a file.
	///
	/// Slow, but ensures that the file hasn't been modified.
	Always,
	/// If the file mtime, size, and (if available) drive+inode are the same as the
	/// previous index, assume the file has not changed and reuse its hash. Otherwise
	/// the file will have their hash computed.
	///
	/// Descent tradeoff between speed and safety, but may miss changes
	/// if the mtime does not change.
	///
	/// Equivalent to `Always` if no previous index was specified.
	IfMetadataChanged,
	/// Never do hashing, and don't transfer hashes from the previous index.
	///
	/// Files are assumed to be unchanged if their size, mtime, and (if available)
	/// drive+inode data have not changed from the previous index.
	///
	/// Fast, but hash info will not be available.
	Never,
}
impl std::str::FromStr for HashMode {
	type Err = &'static str;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s {
			"always" => Ok(Self::Always),
			"if-metadata-changed" => Ok(Self::IfMetadataChanged),
			"never" => Ok(Self::Never),
			_ => Err("Unrecognized hash mode"),
		}
	}
}
