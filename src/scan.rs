use std::{
	fs::Metadata,
	io::Read,
	path::{
		Path,
		PathBuf,
	},
};

use crate::{
	fsid::FSIDCache,
	scan_options::{
		HashMode,
		ScanOptions,
	},
	DirRecord,
	FileRecord,
	Index,
};

#[cfg(target_os = "linux")]
fn inode_of(md: &Metadata) -> Option<u64> {
	use std::os::unix::prelude::MetadataExt;
	Some(md.ino())
}
#[cfg(not(target_os = "linux"))]
fn inode_of(md: &Metadata) -> Option<u64> {
	None
}

pub(crate) fn scan<O: ScanOptions>(
	path: &Path,
	rel_path: &Path,
	options: &O,
) -> Index<O::Digest, O::FileAux, O::DirAux> {
	let mut state = State {
		fsid_cache: FSIDCache::new(),
		file_records: vec![],
		dir_records: vec![],
		hash_mode: options.hash_mode(),
		use_directory_mtime: options.use_directory_mtime(),
		prev_index: options.prev_index(),
		options,
	};

	if let Err(e) = std::fs::metadata(path)
		.map_err(O::Error::from)
		.and_then(|md| scan_rec(path.to_owned(), rel_path, md, &mut state))
	{
		options.on_error(rel_path, e);
	}

	Index::new(state.file_records, state.dir_records)
}

struct State<'a, O: ScanOptions> {
	file_records: Vec<FileRecord<O::Digest, O::FileAux>>,
	dir_records: Vec<DirRecord<O::DirAux>>,
	fsid_cache: FSIDCache,
	hash_mode: HashMode,
	use_directory_mtime: bool,
	prev_index: Option<&'a Index<O::Digest, O::FileAux, O::DirAux>>,
	options: &'a O,
}

fn scan_rec<O: ScanOptions>(
	entry_path: PathBuf,
	rel_path: &Path,
	entry_md: Metadata,
	state: &mut State<O>,
) -> Result<(), O::Error> {
	log::trace!("{:?}: Scanning", entry_path);
	let target_md = entry_md
		.is_symlink()
		.then(|| std::fs::metadata(&entry_path))
		.transpose()?
		.unwrap_or(entry_md);

	if !state.options.should_index(&rel_path, &target_md)? {
		return Ok(());
	}

	let mtime = target_md.modified()?;

	let drive_inode = state
		.fsid_cache
		.get(&entry_path, &target_md)
		.and_then(|id| inode_of(&target_md).map(move |ino| (id, ino)));

	if target_md.is_dir() {
		state.dir_records.push(DirRecord {
			path: rel_path.to_owned(),
			mtime,
			aux: state.options.dir_aux_data(rel_path, &target_md)?,
		});

		if state.use_directory_mtime
			&& state
				.prev_index
				.and_then(|pi| pi.directores_by_path.get(&entry_path))
				.map(|dr| dr.mtime == mtime)
				.unwrap_or(false)
		{
			log::trace!(
				"{:?}: mtime same, assuming contents are the same",
				entry_path
			);
			let prev_index = state.options.prev_index().unwrap();
			state.file_records.extend(
				prev_index
					.by_path
					.range::<PathBuf, _>((&entry_path)..)
					.map(|(_, r)| r)
					.take_while(|rec| !rec.path.starts_with(&entry_path))
					.cloned(),
			);
		} else {
			for entry in std::fs::read_dir(entry_path)? {
				let entry = entry?;
				let next_rel_path = rel_path.join(entry.file_name());

				if let Err(e) = scan_rec(entry.path(), &next_rel_path, entry.metadata()?, state) {
					state.options.on_error(&next_rel_path, e);
				}
			}
		}
		return Ok(());
	}
	if !target_md.is_file() {
		log::trace!("{:?}: Not a dir or file", entry_path);
		return Ok(());
	}

	let size = target_md.len();

	let hash = match (&state.hash_mode, state.prev_index) {
		(HashMode::Always, _) | (HashMode::IfMetadataChanged, None) => {
			log::trace!("{:?}: Hashing", entry_path);
			Some(hash_file::<O::Digest>(&entry_path)?)
		}
		(HashMode::IfMetadataChanged, Some(prev_index)) => {
			let nearest_match = drive_inode
				.as_ref()
				.and_then(|di| prev_index.by_drive_inode(&di.0, di.1).next())
				.or_else(|| prev_index.by_path.get(&entry_path))
				.filter(|pr| pr.size == size && pr.mtime == mtime)
				.and_then(|rec| rec.hash.clone().map(|sha| (rec, sha)));

			if let Some((pi, hash)) = nearest_match {
				log::trace!("{:?}: Assuming hash unchanged from {:?}", entry_path, pi);
				Some(hash)
			} else {
				log::trace!("{:?}: Hashing", entry_path);
				Some(hash_file::<O::Digest>(&entry_path)?)
			}
		}
		(HashMode::Never, _) => None,
	};

	let fr = FileRecord {
		path: rel_path.to_owned(),
		drive_inode,
		mtime,
		size,
		hash,
		aux: state.options.file_aux_data(rel_path, &target_md)?,
	};
	log::debug!("{:?}: {:?}", fr.path, fr);
	state.file_records.push(fr);

	Ok(())
}

fn hash_file<H: digest::Digest>(p: &Path) -> Result<digest::Output<H>, std::io::Error> {
	let mut hash = H::new();
	let mut buf = [0u8; 1024 * 8];
	let mut f = std::fs::File::open(p)?;
	loop {
		let n = f.read(&mut buf)?;
		if n == 0 {
			break;
		}
		hash.update(&buf[..n]);
	}
	Ok(hash.finalize())
}
