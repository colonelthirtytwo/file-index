use std::path::PathBuf;

use clap::Parser;
use file_index::{
	Difference,
	HashMode,
	Index,
};

#[derive(Parser)]
enum Subcommand {
	/// Scan files and create an index, printing it to stdout
	Index {
		/// Scan directory
		dir: PathBuf,
		/// Previous index, in JSON format
		#[clap(short = 'p', long)]
		prev: Option<PathBuf>,
		/// Skip unmodified directories based on their mtime. Only has an effect if
		/// a previous index is specified.
		#[clap(short = 'm', long)]
		use_dir_mtime: bool,

		#[clap(short = 'H', long, default_value = "if-metadata-changed")]
		hash_mode: HashMode,

		/// Print human-readable format
		#[clap(short = 'P', long)]
		pretty: bool,
	},
	/// Prints differences between two indices
	Diff {
		/// Old index, in JSON format
		old: PathBuf,
		/// New index, in JSON format
		new: PathBuf,
	},
}

#[derive(Parser)]
struct Args {
	#[clap(subcommand)]
	command: Subcommand,
}

struct ScanOpts<'a> {
	hash_mode: HashMode,
	use_dir_mtime: bool,
	prev: Option<&'a Index<sha3::Sha3_256>>,
}
impl<'a> file_index::ScanOptions for ScanOpts<'a> {
	type Digest = sha3::Sha3_256;
	type DirAux = ();
	type Error = std::io::Error;
	type FileAux = ();

	fn on_error(&self, rel_path: &std::path::Path, error: Self::Error) {
		log::error!("{}: {}", rel_path.display(), error);
	}

	fn prev_index(&self) -> Option<&Index<Self::Digest, Self::FileAux, Self::DirAux>> {
		self.prev
	}

	fn use_directory_mtime(&self) -> bool {
		self.use_dir_mtime
	}

	fn hash_mode(&self) -> HashMode {
		self.hash_mode
	}

	fn file_aux_data(
		&self,
		_rel_path: &std::path::Path,
		_metadata: &std::fs::Metadata,
	) -> Result<Self::FileAux, Self::Error> {
		Ok(())
	}

	fn dir_aux_data(
		&self,
		_rel_path: &std::path::Path,
		_metadata: &std::fs::Metadata,
	) -> Result<Self::DirAux, Self::Error> {
		Ok(())
	}
}

fn main() -> Result<(), String> {
	env_logger::init();
	match Args::parse().command {
		Subcommand::Index {
			dir,
			prev,
			use_dir_mtime,
			hash_mode,
			pretty,
		} => {
			let old_index = prev
				.map(|path| {
					let f = std::fs::File::open(&path)
						.map_err(|e| format!("Could not open {:?}: {}", path, e))?;
					serde_json::from_reader::<_, Index<sha3::Sha3_256>>(std::io::BufReader::new(f))
						.map_err(|e| format!("Could not read {:?}: {}", path, e))
				})
				.transpose()?;

			let index: Index<sha3::Sha3_256> = Index::scan(
				&dir,
				&ScanOpts {
					hash_mode,
					use_dir_mtime,
					prev: old_index.as_ref(),
				},
			);

			if !pretty {
				serde_json::to_writer_pretty(std::io::stdout().lock(), &index).unwrap();
				println!();
			} else {
				for entry in index.iter() {
					println!(
						"{:?} mtime: {}, size: {}, drive+inode: {}, hash: {}",
						entry.path,
						time::OffsetDateTime::from(entry.mtime),
						entry.size,
						entry
							.drive_inode
							.as_ref()
							.map(|(drive, inode)| format!("{},{}", drive, inode))
							.unwrap_or_else(|| "unavail".into()),
						entry
							.hash
							.as_ref()
							.map(|hash| hex::encode(hash))
							.unwrap_or_else(|| "unavail".into()),
					);
				}
			}
		}
		Subcommand::Diff { old, new } => {
			let old_index = {
				let f = std::fs::File::open(&old)
					.map_err(|e| format!("Could not open {:?}: {}", old, e))?;
				serde_json::from_reader::<_, Index<sha3::Sha3_256>>(std::io::BufReader::new(f))
					.map_err(|e| format!("Could not read {:?}: {}", old, e))?
			};
			let new_index = {
				let f = std::fs::File::open(&new)
					.map_err(|e| format!("Could not open {:?}: {}", new, e))?;
				serde_json::from_reader::<_, Index<sha3::Sha3_256>>(std::io::BufReader::new(f))
					.map_err(|e| format!("Could not read {:?}: {}", new, e))?
			};

			for diff in new_index.diff(&old_index) {
				match diff {
					Difference::FileCreated(path) => {
						let ne = new_index.by_path(path).unwrap();
						println!("{:?}: Created", path);
						if let Some(entry) = old_index.by_same_content(ne) {
							println!("\t- Contents copied/moved from {:?}", entry.path);
						}
					}
					Difference::FileRemoved(path) => {
						let oe = old_index.by_path(path).unwrap();
						println!("{:?}: Removed", path);
						if let Some(entry) = new_index.by_same_content(oe) {
							println!("\t- Contents still available at {:?}", entry.path);
						}
					}
					Difference::FileModified(path) => {
						let oe = old_index.by_path(path).unwrap();
						let ne = new_index.by_path(path).unwrap();
						println!("{:?}: Modified", path);

						if let Some(entry) = new_index.by_same_content(oe) {
							println!("\t- Old contents still avilable at {:?}", entry.path)
						}
						if let Some(entry) = old_index.by_same_content(ne) {
							println!("\t- Contents moved/copied from {:?}", entry.path);
						}
					}
				}
			}
		}
	}
	Ok(())
}
