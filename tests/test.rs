use std::{path::{
	Path,
	PathBuf,
}};

use file_index::{
	Difference,
	FileRecord,
	Index,
};
use temp_dir::TempDir;

fn sha3hex(str: &str) -> digest::Output<H> {
	let v = hex::decode(str).unwrap();
	let mut h = digest::Output::<H>::default();
	h.copy_from_slice(&v);
	h
}

type H = sha3::Sha3_224;
type TestIndex = Index<H, u64>;

struct ScanOpts<'a> {
	hash_mode: file_index::HashMode,
	use_dir_mtime: bool,
	prev: Option<&'a Index<H, u64>>,
}
impl<'a> file_index::ScanOptions for ScanOpts<'a> {
	type Digest = H;
	type Error = std::io::Error;
	type DirAux = ();
	type FileAux = u64;

	fn on_error(&self, rel_path: &std::path::Path, error: Self::Error) {
		panic!("{}: {}", rel_path.display(), error);
	}

	fn prev_index(&self) -> Option<&Index<Self::Digest, Self::FileAux, Self::DirAux>> {
		self.prev
	}

	fn use_directory_mtime(&self) -> bool {
		self.use_dir_mtime
	}

	fn hash_mode(&self) -> file_index::HashMode {
		self.hash_mode
	}

	fn file_aux_data(
		&self,
		_rel_path: &std::path::Path,
		metadata: &std::fs::Metadata,
	) -> Result<Self::FileAux, Self::Error> {
		Ok(metadata.len())
	}

	fn dir_aux_data(
		&self,
		_rel_path: &std::path::Path,
		_metadata: &std::fs::Metadata,
	) -> Result<Self::DirAux, Self::Error> {
		Ok(())
	}
}

#[test]
fn test_indexing() {
	let start_time = std::time::SystemTime::now() - std::time::Duration::from_secs(1);

	let d = TempDir::new().unwrap();
	std::fs::write(d.child("a.txt"), b"Hello world!").unwrap();
	std::fs::write(d.child("b.txt"), b"Goodbye world!").unwrap();
	std::fs::create_dir(d.child("c")).unwrap();
	std::fs::write(d.child("c/d.txt"), b"Lorem ipsum").unwrap();

	let index = Index::scan(
		d.path(),
		&ScanOpts {
			hash_mode: file_index::HashMode::Always,
			use_dir_mtime: false,
			prev: None,
		},
	);

	let expected = vec![
		FileRecord::<H, u64> {
			path: "a.txt".into(),
			mtime: std::time::UNIX_EPOCH,
			size: b"Hello world!".len() as u64,
			drive_inode: None,
			hash: Some(sha3hex(
				"d3ee9b1ba1990fecfd794d2f30e0207aaa7be5d37d463073096d86f8",
			)),
			aux: b"Hello world!".len() as u64,
		},
		FileRecord {
			path: "b.txt".into(),
			mtime: std::time::UNIX_EPOCH,
			size: b"Goodbye world!".len() as u64,
			drive_inode: None,
			hash: Some(sha3hex(
				"2561e743d274747e8c93ad451cdba602595abfaef9abddd3f20b6b9c",
			)),
			aux: b"Goodbye world!".len() as u64,
		},
		FileRecord {
			path: "c/d.txt".into(),
			mtime: std::time::UNIX_EPOCH,
			size: b"Lorem ipsum".len() as u64,
			drive_inode: None,
			hash: Some(sha3hex(
				"42528b3b9996050e2250d2854b37fd498e5691b56bc976708a401eba",
			)),
			aux: b"Lorem ipsum".len() as u64,
		},
	];

	assert_eq!(expected.len(), index.len());

	for (expected, actual) in expected.iter().zip(index.iter()) {
		assert_eq!(expected.path, actual.path);
		assert_eq!(expected.size, actual.size);
		assert_eq!(expected.hash, actual.hash);
		assert_eq!(expected.aux, actual.aux);
		assert!(
			actual.mtime > start_time,
			"File mtime of {:?} <= start time of {:?}",
			actual.mtime,
			start_time
		);
	}

	assert_eq!(
		index
			.by_digest(&sha3hex(
				"d3ee9b1ba1990fecfd794d2f30e0207aaa7be5d37d463073096d86f8"
			))
			.map(|r| r.path.clone())
			.collect::<Vec<PathBuf>>(),
		vec![PathBuf::from("a.txt")]
	);

	assert_eq!(
		index
			.by_digest(&sha3hex(
				"2561e743d274747e8c93ad451cdba602595abfaef9abddd3f20b6b9c"
			))
			.map(|r| r.path.clone())
			.collect::<Vec<PathBuf>>(),
		vec![PathBuf::from("b.txt")]
	);

	assert_eq!(
		index
			.by_digest(&sha3hex(
				"42528b3b9996050e2250d2854b37fd498e5691b56bc976708a401eba"
			))
			.map(|r| r.path.clone())
			.collect::<Vec<PathBuf>>(),
		vec![PathBuf::from("c/d.txt")]
	);

	assert_eq!(index.directories().len(), 2);
	assert!(index.directories().contains_key(Path::new("")));
	assert!(index.directories().contains_key(Path::new("c")));
}

#[test]
fn test_changed() {
	let d = TempDir::new().unwrap();
	std::fs::write(d.child("a.txt"), b"Hello world!").unwrap();
	std::fs::write(d.child("b.txt"), b"Goodbye world!").unwrap();

	let options = ScanOpts {
		hash_mode: file_index::HashMode::Always,
		use_dir_mtime: false,
		prev: None,
	};

	let index1 = TestIndex::scan(d.path(), &options);

	std::fs::write(d.child("a.txt"), "Goodbye world!").unwrap();

	let index2 = TestIndex::scan(d.path(), &options);

	let diff = index2.diff(&index1).collect::<Vec<_>>();
	assert_eq!(diff, vec![Difference::FileModified(Path::new("a.txt"))]);

	assert_eq!(
		index2.by_same_content(index1.by_path("b.txt").unwrap()),
		Some(index1.by_path("b.txt").unwrap())
	);
	assert_eq!(
		index2.by_same_content(index1.by_path("a.txt").unwrap()),
		None
	);

	assert_eq!(index2.directories().len(), 1);
	assert!(index2.directories().contains_key(Path::new("")));
}

#[test]
fn test_changed_use_prev() {
	let d = TempDir::new().unwrap();
	std::fs::write(d.child("a.txt"), b"Hello world!").unwrap();
	std::fs::write(d.child("b.txt"), b"Goodbye world!").unwrap();

	let options = ScanOpts {
		hash_mode: file_index::HashMode::Always,
		use_dir_mtime: false,
		prev: None,
	};

	let index1 = TestIndex::scan(d.path(), &options);

	std::fs::write(d.child("a.txt"), "Goodbye world!").unwrap();

	let options = ScanOpts {
		prev: Some(&index1),
		..options
	};

	let index2 = TestIndex::scan(d.path(), &options);

	let diff = index2.diff(&index1).collect::<Vec<_>>();
	assert_eq!(diff, vec![Difference::FileModified(Path::new("a.txt"))]);

	assert_eq!(
		index2.by_same_content(index1.by_path("b.txt").unwrap()),
		Some(index1.by_path("b.txt").unwrap())
	);
	assert_eq!(
		index2.by_same_content(index1.by_path("a.txt").unwrap()),
		None
	);

	assert_eq!(index2.directories().len(), 1);
	assert!(index2.directories().contains_key(Path::new("")));
}
